let SOURCE = []

// menoresDeEdad  x < 18  "blue"
// mayoresDeEdad  x > 18 & <= 54 "red"
// terceraEdad    x >55 "green"

fetch(`./DATA.json`)
   .then(response => {
      return response.json();
   })
   .then(jsondata => {
      console.log({ jsondata })
      SOURCE = jsondata
      let ARRAY_OUTPUT = SOURCE.map(item => {
         let salida = "";
         if (item.Edad < 18) salida = "blue";
         else if (item.Edad > 18 && item.Edad <= 54) salida = "red";
         else if (item.Edad > 55) salida = "green"
         return {
            x: item.Nombre,
            y: item.Edad,
            color: salida
         };
      });
      console.log({ARRAY_OUTPUT, SOURCE});
   })
   .catch(function(error) {
      console.error("Error al cargar el archivo JSON:", error);
   });

let config_grafic = {

   width: 700,
   height: 500,
   margin: { top: 20, right: 20, bottom: 120, left: 40 }

};

var xScale = d3.scaleBand()
   .domain(SOURCE.map(function (item) { return item.Nombre; }))
   .range([config_grafic.margin.left, config_grafic.width - config_grafic.margin.right])
   .padding(0.5);

var yScale = d3.scaleLinear()
   .domain([0, d3.max(SOURCE, function (item) { return item.Edad; })])
   .range([config_grafic.height - config_grafic.margin.bottom, config_grafic.margin.top]);


var svg = d3.select("#columna")
   .attr("width", config_grafic.width + config_grafic.margin.left + config_grafic.margin.right)
   .attr("height", config_grafic.height + config_grafic.margin.top + config_grafic.margin.bottom);



svg.selectAll("rect")
   .data(SOURCE)
   .enter()
   .append("rect")
   .attr("x", function (item) { return xScale(item.Nombre); })
   .attr("y", function (item) { return yScale(item.Edad); })
   .attr("width", xScale.bandwidth())
   .attr("height", function (d) { return config_grafic.height - config_grafic.margin.bottom - yScale(d.Edad); })
   .attr("fill", function(item){ return item.color})

svg.append("g")
   .attr("transform", "translate(0," + (config_grafic.height - config_grafic.margin.bottom) + ")")
   .call(d3.axisBottom(xScale))
   .selectAll("text")
   .style("text-anchor", "end")
   .attr("dx", "-0.8em")
   .attr("dy", "0.15em")
   .attr("transform", "rotate(-90)");

svg.append("g")
   .attr("transform", "translate(" + config_grafic.margin.left + ",0)")
   .call(d3.axisLeft(yScale));