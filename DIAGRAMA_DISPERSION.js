d3.json("DATA.json").then(function(data) {

  // Dimensiones del gráfico
  var width = 600;
  var height = 800;
  var margin = { top: 10, right: 20, bottom: 120, left: 120 };
  
  // Escalas
  var xScale = d3.scaleLinear()
      .domain([0, d3.max(data, function(d) { return d.Edad; })])
      .range([margin.left, width - margin.right]);
  
  var yScale = d3.scaleBand()
      .domain(data.map(function(d) { return d.Nombre; }))
      .range([margin.top, height - margin.bottom])
      .padding(0.1);
  
  // Crear el SVG
  var svg = d3.select("#scatter-plot")
      .attr("width", width)
      .attr("height", height);

      const estilo = {
        fill_mayor: "green",
        fill_menor: "purple",
        stroke: "none"
     };

  // Agregar puntos de datos
  svg.selectAll("circle")
      .data(data)
      .enter().append("circle")
      .attr("class", "dot")
      .attr("cx", function(d) { return xScale(d.Edad); })
      .attr("cy", function(d) { return yScale(d.Nombre); })
      .attr("r", 5)
      .attr("fill", function(d){
        if (d.Edad > 30) {
           return estilo.fill_mayor;
        } else {
           return estilo.fill_menor;
        }
     });
  
  // Agregar ejes
  svg.append("g")
      .attr("transform", "translate(0," + (height - margin.bottom) + ")")
      .call(d3.axisBottom(xScale));
  
  svg.append("g")
      .attr("transform", "translate(" + margin.left + ",0)")
      .call(d3.axisLeft(yScale));
 
    }).catch(function(error) {
        console.error("Error al cargar el archivo JSON:", error);
      });